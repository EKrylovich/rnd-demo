package rnd.recommentation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by ekrylovich
 * on 20.11.17.
 */
@SpringBootApplication
@EnableDiscoveryClient
public class RecommendationApplication {
    public static void main(String[] args){
        SpringApplication.run(RecommendationApplication.class, args);
    }
}
