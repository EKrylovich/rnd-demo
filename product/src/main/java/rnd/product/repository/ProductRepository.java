package rnd.product.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rnd.product.model.Product;

import java.util.List;

/**
 * Created by ekrylovich
 * on 24.11.17.
 */
public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findByName(String name);
}
