package rnd.product.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rnd.product.model.Product;

/**
 * Created by ekrylovich
 * on 20.11.17.
 */
@RestController
public class ProductController {

    @RequestMapping("/product/{productId}")
    public Product getProduct(@PathVariable int productId){
        return  new Product(productId, "name", 90);
    }
}
