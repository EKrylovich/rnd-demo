package rnd.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import rnd.product.model.Product;
import rnd.product.repository.ProductRepository;

/**
 * Created by ekrylovich
 * on 20.11.17.
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ProductApplication {
    private static final Logger log = LoggerFactory.getLogger(ProductApplication.class);

    public static void main(String[] args){
        SpringApplication.run(ProductApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(ProductRepository repository) {
        return (args) -> {
            // save a couple of customers
            repository.save(new Product("Box", 1));
            repository.save(new Product("Rap-box", 2));
            repository.save(new Product("Rock-box", 3));

            // fetch all customers
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (Product customer : repository.findAll()) {
                log.info(customer.toString());
            }
            log.info("");

            // fetch an individual customer by ID
            Product customer = repository.findOne(1L);
            log.info("Customer found with findOne(1L):");
            log.info("--------------------------------");
            log.info(customer.toString());
            log.info("");

            // fetch customers by last name
            log.info("Customer found with findByLastName('Rap-box'):");
            log.info("--------------------------------------------");
            for (Product bauer : repository.findByName("Rap-box")) {
                log.info(bauer.toString());
            }
            log.info("");
        };
    }

}
