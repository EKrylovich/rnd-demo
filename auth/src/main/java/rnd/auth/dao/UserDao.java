package rnd.auth.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import rnd.auth.model.User;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by ekrylovich
 * on 24.11.17.
 */
@Component
public class UserDao{

    public static final String SELECT_USER_QUERY = "SELECT id, user_name, password FROM user WHERE user_name = ?";
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public User findUserByName(String userName){
        return jdbcTemplate.queryForObject(
                SELECT_USER_QUERY, new Object[]{userName},
                getRowMapper());
    }

    private RowMapper<User> getRowMapper(){
        return (rs, rowNum) ->
                new User(rs.getLong("id"), rs.getString("user_name"), rs.getString("password"));
    }
}
