package rnd.auth.model;

/**
 * Created by ekrylovich
 * on 24.11.17.
 */
public class User {
    private long id;
    private String userName;
    private String password;

    public User(long id, String user, String password) {
        this.id = id;
        this.userName = user;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
